package assignmentwork;

public class Swapping {

	public static void main(String[] args) {
		int a,b;
		a = 10;
		b = 20;
		System.out.println("Before Swapping:");
		System.out.println("Value of a is:" +a);
		System.out.println("Value of b is:" +b);
		a = a+b;
		b = a-b;
		a = a-b;
		System.out.println("---------------------------- ");
		System.out.println("After Swapping:");
		System.out.println("Value of a is:" +a);
		System.out.println("Value of b is:" +b);
	}

}
