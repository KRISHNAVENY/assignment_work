package assignmentwork;

import java.util.Scanner;

public class Palindrome {

	public static void main(String[] args) {
		String st;
		String rev ="";
		System.out.println("Enter the string:");
		Scanner sc = new Scanner(System.in);
		st = sc.nextLine();
		int length = st.length();
		for(int i = length -1;i>=0;i--)
		{
			rev = rev +st.charAt(i);
		}
		if(st.equalsIgnoreCase(rev))
		{
			System.out.println("String is a palindrome");
		}
		else
		{
			System.out.println("String is not a palindrome");
		}
		sc.close();	
	}

}
